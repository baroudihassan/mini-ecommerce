# MiniEcommerce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.1.1.

# Live demo URL

https://baroudihassan.gitlab.io/mini-ecommerce


## Setup instructions

Clone the project

Jump to the project folder `cd mini-ecommerce`

Install the dependencies `npm install`

Start the project `npm start`

Navigate to `http://localhost:4200/`


## Feature list

- Fetch and display a list of products

- Search for products by name and filter by category

- Click on a product to see more detailed view

- Add product to my shopping cart with success or error message

- View the shopping cart

- Adjust product quantity, and remove products from the cart with a confirmation modal

- Checkout page form where users can enter mock payment and shipping information to complete their order

- Responsive design that adapts well across devices


## Architectural overview

📦src<br/>
 ┣ 📂app<br/>
 ┃ ┣ 📂components<br/>
 ┃ ┃ ┣ 📂cart-item<br/>
 ┃ ┃ ┃ ┣ 📜cart-item.component.html<br/>
 ┃ ┃ ┃ ┣ 📜cart-item.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜cart-item.component.ts<br/>
 ┃ ┃ ┣ 📂confirm-modal<br/>
 ┃ ┃ ┃ ┣ 📜confirm-modal.component.html<br/>
 ┃ ┃ ┃ ┣ 📜confirm-modal.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜confirm-modal.component.ts<br/>
 ┃ ┃ ┣ 📂navbar<br/>
 ┃ ┃ ┃ ┣ 📜navbar.component.html<br/>
 ┃ ┃ ┃ ┣ 📜navbar.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜navbar.component.ts<br/>
 ┃ ┃ ┣ 📂product-item<br/>
 ┃ ┃ ┃ ┣ 📜product-item.component.html<br/>
 ┃ ┃ ┃ ┣ 📜product-item.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜product-item.component.ts<br/>
 ┃ ┃ ┣ 📂product-quantity<br/>
 ┃ ┃ ┃ ┣ 📜product-quantity.component.html<br/>
 ┃ ┃ ┃ ┣ 📜product-quantity.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜product-quantity.component.ts<br/>
 ┃ ┃ ┗ 📂toast<br/>
 ┃ ┃ ┃ ┣ 📜toast.component.html<br/>
 ┃ ┃ ┃ ┣ 📜toast.component.ts<br/>
 ┃ ┃ ┃ ┗ 📜toast.service.ts<br/>
 ┃ ┣ 📂interfaces<br/>
 ┃ ┃ ┣ 📜product.interface.ts<br/>
 ┃ ┃ ┗ 📜response.interface.ts<br/>
 ┃ ┣ 📂pages<br/>
 ┃ ┃ ┣ 📂cart-page<br/>
 ┃ ┃ ┃ ┣ 📜cart-page.component.html<br/>
 ┃ ┃ ┃ ┣ 📜cart-page.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜cart-page.component.ts<br/>
 ┃ ┃ ┣ 📂checkout-page<br/>
 ┃ ┃ ┃ ┣ 📜checkout-page.component.html<br/>
 ┃ ┃ ┃ ┣ 📜checkout-page.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜checkout-page.component.ts<br/>
 ┃ ┃ ┣ 📂product-details-page<br/>
 ┃ ┃ ┃ ┣ 📜product-details-page.component.html<br/>
 ┃ ┃ ┃ ┣ 📜product-details-page.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜product-details-page.component.ts<br/>
 ┃ ┃ ┗ 📂products-page<br/>
 ┃ ┃ ┃ ┣ 📜products-page.component.html<br/>
 ┃ ┃ ┃ ┣ 📜products-page.component.scss<br/>
 ┃ ┃ ┃ ┗ 📜products-page.component.ts<br/>
 ┃ ┣ 📂services<br/>
 ┃ ┃ ┣ 📜cart.service.ts<br/>
 ┃ ┃ ┗ 📜product.service.ts<br/>
 ┃ ┣ 📜app.component.html<br/>
 ┃ ┣ 📜app.component.scss<br/>
 ┃ ┣ 📜app.component.spec.ts<br/>
 ┃ ┣ 📜app.component.ts<br/>
 ┃ ┣ 📜app.config.ts<br/>
 ┃ ┗ 📜app.routes.ts<br/>
 ┣ 📂assets<br/>
 ┃ ┗ 📜.gitkeep<br/>
 ┣ 📂environments<br/>
 ┃ ┣ 📜environment.development.ts<br/>
 ┃ ┗ 📜environment.ts<br/>
 ┣ 📜favicon.ico<br/>
 ┣ 📜index.html<br/>
 ┣ 📜main.ts<br/>
 ┗ 📜styles.scss<br/>
 
