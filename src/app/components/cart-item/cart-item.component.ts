import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CartProduct } from '../../interfaces/product.interface';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductQuantityComponent } from '../product-quantity/product-quantity.component';

@Component({
  selector: 'app-cart-item',
  standalone: true,
  imports: [
    CommonModule,
    NgbRatingModule,
    ProductQuantityComponent
  ],
  templateUrl: './cart-item.component.html',
  styleUrl: './cart-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartItemComponent {
  @Input() cartProduct!: CartProduct;
  @Output() removeFromCart: EventEmitter<void> = new EventEmitter<void>();
  @Output() updateQuantity = new EventEmitter<number>();

  onRemoveFromCart(): void {
    this.removeFromCart.emit();
  }

  onQuantityChange(quantity: number): void {
    this.updateQuantity.emit(quantity);
  }
}
