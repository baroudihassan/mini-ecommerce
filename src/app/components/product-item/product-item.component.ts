import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, output } from '@angular/core';
import { Product } from '../../interfaces/product.interface';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product-item',
  standalone: true,
  imports: [
    CommonModule,
    NgbRatingModule
  ],
  templateUrl: './product-item.component.html',
  styleUrl: './product-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductItemComponent {
  @Input() product!: Product;
  @Output() addToCart: EventEmitter<void> = new EventEmitter<void>();

  onAddToCart(event: Event) {
    event.stopPropagation(); // Prevent the click event from propagating to the parent product div
    this.addToCart.emit();
  }
}
