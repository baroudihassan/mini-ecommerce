import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-product-quantity',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule
  ],
  templateUrl: './product-quantity.component.html',
  styleUrl: './product-quantity.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductQuantityComponent {
  @Input() quantity: number = 1;
  @Output() quantityChange = new EventEmitter<number>();

  decreaseQuantity(): void {
    if (this.quantity > 1) {
      this.quantity--;
      this.quantityChange.emit(this.quantity);
    }
  }

  increaseQuantity(): void {
    this.quantity++;
    this.quantityChange.emit(this.quantity);
  }
}
