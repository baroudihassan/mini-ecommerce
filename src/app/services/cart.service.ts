import { Injectable } from '@angular/core';
import { CartProduct, Product } from '../interfaces/product.interface';
import { Response } from '../interfaces/response.interface'

const CART_KEY = 'cartItems';

@Injectable({ providedIn: 'root' })
export class CartService {

  constructor() { }

  get cartProducts(): CartProduct[] {
    return JSON.parse(localStorage.getItem(CART_KEY) || '[]');
  }

  get totalPrice(): number {
    return this.cartProducts.reduce((total, cartProduct) => total + (cartProduct.price * cartProduct.quantity), 0);
  }

  addToCart(product: Product, quantity: number = 1): Response {
    const response: Response = { success: true, message: '' };
    const products = this.cartProducts;
    const existingProduct = products.find(cartProduct => cartProduct.id === product.id);

    if (existingProduct) {
      response.success = false;
      response.message = "Product already in cart";
    } else {
      products.push({ ...product, quantity });
    }

    this.saveProducts(products);
    return response;
  }

  removeFromCart(productId: number): void {
    const productsToKeep = this.cartProducts.filter(p => p.id !== productId);
    this.saveProducts(productsToKeep);
  }

  updateProductQuantity(productId: number, quantity: number): void {
    const updatedProducts = this.cartProducts.map(cartProduct => {
      if (cartProduct.id === productId) {
        cartProduct.quantity = quantity;
      }
      return cartProduct;
    });

    this.saveProducts(updatedProducts);
  }

  private saveProducts(cartProducts: CartProduct[]): void {
    localStorage.setItem(CART_KEY, JSON.stringify(cartProducts));
  }

}
