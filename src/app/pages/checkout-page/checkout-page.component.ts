import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ToastService } from '../../components/toast/toast.service';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-checkout-page',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './checkout-page.component.html',
  styleUrl: './checkout-page.component.scss',
})
export class CheckoutPageComponent {
  toastService = inject(ToastService);
  checkoutForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private cartService: CartService) {
    this.initForm();
  }

  private initForm(): void {
    this.checkoutForm = this.formBuilder.group({
      shippingInfo: this.formBuilder.group({
        fullName: ['', Validators.required],
        address: ['', Validators.required],
        city: ['', Validators.required],
        postalCode: ['', Validators.required]
      }),
      paymentInfo: this.formBuilder.group({
        cardNumber: ['', Validators.required],
        expiry: ['', Validators.required]
      })
    });
  }

  onSubmit(): void {
    this.checkoutForm.markAllAsTouched();
    if (this.checkoutForm.valid) {
      console.log('Form data:', this.checkoutForm.value);
      this.toastService.show({ content: 'Your order has been sent!', classname: 'bg-success text-light', delay: 1000 });
    } else {
      this.toastService.show({ content: 'Invalid form!', classname: 'bg-danger text-light', delay: 1000 });
    }
  }

  get totalPrice(): number {
    return this.cartService.totalPrice;
  }
}
