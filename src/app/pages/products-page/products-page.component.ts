import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { debounceTime, finalize, map, startWith, switchMap } from 'rxjs/operators'
import { Product } from '../../interfaces/product.interface';
import { ProductService } from '../../services/product.service';
import { ProductItemComponent } from '../../components/product-item/product-item.component';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../services/cart.service';
import { ToastService } from '../../components/toast/toast.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-products-page',
  standalone: true,
  imports: [
    CommonModule,
    ProductItemComponent,
    NgbProgressbarModule,
    ReactiveFormsModule
  ],
  templateUrl: './products-page.component.html',
  styleUrl: './products-page.component.scss'
})
export class ProductsPageComponent implements OnInit {
  toastService = inject(ToastService);
  isLoading = false;
  products$!: Observable<Product[]>;
  filteredProducts$!: Observable<Product[]>;
  categories$!: Observable<string[]>;
  searchForm = new FormGroup({
    term: new FormControl(''),
    category: new FormControl('')
  });

  constructor(private productService: ProductService, private cartService: CartService, private router: Router) { }

  ngOnInit(): void {
    this.initCategories();
    this.initProducts();
  }

  initProducts(): void {
    this.isLoading = true;
    this.products$ = this.productService.getProducts().pipe(finalize(() => this.isLoading = false));
    this.filteredProducts$ = combineLatest([
      this.products$,
      this.searchForm.get('term')!.valueChanges.pipe(startWith('')),
      this.searchForm.get('category')!.valueChanges.pipe(startWith(''))
    ]).pipe(
      debounceTime(200),
      map(([products, term, category]) => {
        return products.filter(product => {
          const termMatch = term ? product.title.toLowerCase().includes(term.toLowerCase()) : true;
          const categoryMatch = category ? product.category === category : true;
          return termMatch && categoryMatch;
        });
      })
    );
  }

  initCategories(): void {
    this.isLoading = true;
    this.categories$ = this.productService.getCategories().pipe(finalize(() => this.isLoading = false));
  }

  addToCart(product: Product): void {
    const response = this.cartService.addToCart(product);
    if (!response.success) {
      this.toastService.show({ content: response.message, classname: 'bg-danger text-light', delay: 1000 });
    } else {
      this.toastService.show({ content: 'Product Added to cart', classname: 'bg-success text-light', delay: 1000 });
    }
  }

  goToDetails(productId: number): void {
    this.router.navigate(['products', productId]);
  }
}
