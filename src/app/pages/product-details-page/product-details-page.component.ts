import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Product } from '../../interfaces/product.interface';
import { Observable } from 'rxjs';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../services/cart.service';
import { ToastService } from '../../components/toast/toast.service';

@Component({
  selector: 'app-product-details-page',
  standalone: true,
  imports: [
    CommonModule,
    NgbRatingModule
  ],
  templateUrl: './product-details-page.component.html',
  styleUrl: './product-details-page.component.scss'
})
export class ProductDetailsPageComponent implements OnInit {
  toastService = inject(ToastService);
  product$!: Observable<Product>;

  constructor(private route: ActivatedRoute, private productService: ProductService, private cartService: CartService) { }

  ngOnInit(): void {
    this.loadProduct();
  }

  private loadProduct(): void {
    const productId = this.route.snapshot.paramMap.get('id');
    if (productId) {
      this.product$ = this.productService.getProduct(+productId);
    }
  }

  addToCart(product: Product): void {
    const response = this.cartService.addToCart(product);
    if (!response.success) {
      this.toastService.show({ content: response.message, classname: 'bg-danger text-light', delay: 1000 });
    } else {
      this.toastService.show({ content: 'Product Added to cart', classname: 'bg-success text-light', delay: 1000 });
    }
  }
}
