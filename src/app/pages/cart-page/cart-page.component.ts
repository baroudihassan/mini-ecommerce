import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { CartProduct } from '../../interfaces/product.interface';
import { CartItemComponent } from '../../components/cart-item/cart-item.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../../components/confirm-modal/confirm-modal.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-cart-page',
  standalone: true,
  imports: [
    CommonModule,
    CartItemComponent,
    RouterModule
  ],
  templateUrl: './cart-page.component.html',
  styleUrl: './cart-page.component.scss'
})
export class CartPageComponent implements OnInit {
  private modalService = inject(NgbModal);
  cartProducts: CartProduct[] = [];

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.initCartProducts();
  }

  private initCartProducts() {
    this.cartProducts = this.cartService.cartProducts;
  }

  removeFromCart(cartProduct: CartProduct): void {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.message = 'Do you want to delete this product?';
    modalRef.closed.subscribe(isOk => {
      if (isOk) {
        this.cartService.removeFromCart(cartProduct.id);
        this.initCartProducts();
      }
    })
  }

  updateProductQuantity(productId: number, quantity: number): void {
    this.cartService.updateProductQuantity(productId, quantity);
    this.initCartProducts();
  }

  get totalPrice(): number {
    return this.cartService.totalPrice;
  }
}
